// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:Unlit/Texture,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:True,atwp:True,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:9361,x:33622,y:32737,varname:node_9361,prsc:2|normal-9121-RGB,custl-3392-OUT,olwid-2720-OUT,olcol-4403-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:5928,x:30649,y:32166,varname:node_5928,prsc:2;n:type:ShaderForge.SFN_LightColor,id:9725,x:31004,y:32297,varname:node_9725,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5127,x:31263,y:32178,varname:node_5127,prsc:2|A-2721-OUT,B-9725-RGB;n:type:ShaderForge.SFN_Add,id:4759,x:31492,y:32125,varname:node_4759,prsc:2|A-9637-OUT,B-5127-OUT;n:type:ShaderForge.SFN_Clamp01,id:8839,x:31682,y:32125,varname:node_8839,prsc:2|IN-4759-OUT;n:type:ShaderForge.SFN_Set,id:3955,x:31889,y:32125,varname:DirectLighting,prsc:2|IN-8839-OUT;n:type:ShaderForge.SFN_Tex2d,id:9212,x:28930,y:32339,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2012,x:32687,y:33920,varname:node_2012,prsc:2|A-2326-OUT,B-8648-OUT;n:type:ShaderForge.SFN_Color,id:8259,x:32360,y:34635,ptovrint:False,ptlb:Outline Color,ptin:_OutlineColor,cmnt:--GroupMember Outline,varname:_OutlineColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8382353,c2:0.8382353,c3:0.8382353,c4:1;n:type:ShaderForge.SFN_Slider,id:8648,x:32306,y:34024,ptovrint:False,ptlb:Outline Width,ptin:_OutlineWidth,cmnt:--GroupMember Outline,varname:_OutlineWidth,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.001;n:type:ShaderForge.SFN_Lerp,id:8258,x:32658,y:34555,varname:node_8258,prsc:2|A-3054-OUT,B-8259-RGB,T-8893-OUT;n:type:ShaderForge.SFN_Slider,id:8893,x:32282,y:34817,ptovrint:False,ptlb:Outline Blend Color,ptin:_OutlineBlendColor,cmnt:--GroupMember Outline,varname:_OutlineBlendColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Dot,id:3597,x:31093,y:33481,varname:node_3597,prsc:2,dt:4|A-3544-OUT,B-8879-OUT;n:type:ShaderForge.SFN_Multiply,id:5508,x:31667,y:33235,varname:node_5508,prsc:2|A-4075-OUT,B-8416-OUT;n:type:ShaderForge.SFN_Add,id:8430,x:31838,y:33235,varname:node_8430,prsc:2|A-7963-OUT,B-5508-OUT;n:type:ShaderForge.SFN_LightVector,id:8879,x:30895,y:33542,varname:node_8879,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5588,x:33053,y:32290,varname:node_5588,prsc:2|A-7067-OUT,B-8653-OUT;n:type:ShaderForge.SFN_Get,id:8653,x:32775,y:32437,varname:node_8653,prsc:2|IN-3955-OUT;n:type:ShaderForge.SFN_Set,id:5593,x:33418,y:34559,varname:OutlineColor,prsc:2|IN-9324-OUT;n:type:ShaderForge.SFN_Get,id:4403,x:33217,y:33133,varname:node_4403,prsc:2|IN-5593-OUT;n:type:ShaderForge.SFN_RgbToHsv,id:3116,x:31769,y:34413,varname:node_3116,prsc:2|IN-91-OUT;n:type:ShaderForge.SFN_HsvToRgb,id:3054,x:32422,y:34428,varname:node_3054,prsc:2|H-7077-OUT,S-3116-SOUT,V-3116-VOUT;n:type:ShaderForge.SFN_Slider,id:8075,x:31612,y:34580,ptovrint:False,ptlb:Outline Color Hue Shift,ptin:_OutlineColorHueShift,cmnt:--GroupMember Outline,varname:_OutlineColorHueShift,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Set,id:1495,x:32854,y:33920,varname:OutlineWidth,prsc:2|IN-2012-OUT;n:type:ShaderForge.SFN_Get,id:3308,x:32838,y:33027,varname:node_3308,prsc:2|IN-1495-OUT;n:type:ShaderForge.SFN_Code,id:2720,x:33078,y:33009,varname:node_2720,prsc:2,code:cgBlAHQAdQByAG4AIAB0AG8AZwBnAGwAZQAgAD8AIAB2AGEAbAB1AGUAIAA6ACAAMAAuADAAZgA7AA==,output:0,fname:enableoutlinecode,width:294,height:112,input:8,input:0,input_1_label:toggle,input_2_label:value|A-2083-OUT,B-3308-OUT;n:type:ShaderForge.SFN_ToggleProperty,id:2083,x:32838,y:32962,ptovrint:False,ptlb:Enable Outline,ptin:_EnableOutline,cmnt:--Group Outline,varname:_EnableOutline,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_Set,id:6998,x:32187,y:33235,varname:Shade,prsc:2|IN-8091-OUT;n:type:ShaderForge.SFN_Get,id:9451,x:31854,y:30752,varname:node_9451,prsc:2|IN-6998-OUT;n:type:ShaderForge.SFN_Set,id:253,x:29441,y:32335,varname:MainTexture,prsc:2|IN-9212-RGB;n:type:ShaderForge.SFN_Get,id:9077,x:31896,y:31095,varname:node_9077,prsc:2|IN-253-OUT;n:type:ShaderForge.SFN_Get,id:91,x:31560,y:34413,varname:node_91,prsc:2|IN-253-OUT;n:type:ShaderForge.SFN_Add,id:1920,x:32023,y:34330,varname:node_1920,prsc:2|A-3116-HOUT,B-8075-OUT;n:type:ShaderForge.SFN_Fmod,id:7077,x:32221,y:34330,varname:node_7077,prsc:2|A-1920-OUT,B-6450-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6450,x:32023,y:34541,ptovrint:False,ptlb:Outline Color Hue Shift Mod,ptin:_OutlineColorHueShiftMod,varname:_OutlineColorHueShiftMod,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Set,id:1832,x:33851,y:32294,varname:Lighting,prsc:2|IN-6169-OUT;n:type:ShaderForge.SFN_Get,id:3392,x:33217,y:32932,varname:node_3392,prsc:2|IN-1832-OUT;n:type:ShaderForge.SFN_Get,id:7067,x:32775,y:32361,varname:node_7067,prsc:2|IN-5084-OUT;n:type:ShaderForge.SFN_Set,id:5084,x:34108,y:31839,varname:ShadowedMainTexture,prsc:2|IN-160-OUT;n:type:ShaderForge.SFN_ViewVector,id:9678,x:31388,y:32751,varname:node_9678,prsc:2;n:type:ShaderForge.SFN_OneMinus,id:3475,x:31902,y:32707,varname:node_3475,prsc:2|IN-1981-OUT;n:type:ShaderForge.SFN_Set,id:3449,x:33077,y:32636,varname:RimLight,prsc:2|IN-4258-OUT;n:type:ShaderForge.SFN_Get,id:9793,x:33294,y:32008,varname:node_9793,prsc:2|IN-3449-OUT;n:type:ShaderForge.SFN_Add,id:5714,x:33571,y:31907,varname:node_5714,prsc:2|A-6708-OUT,B-9793-OUT;n:type:ShaderForge.SFN_Color,id:3189,x:32585,y:32816,ptovrint:False,ptlb:RimLight Color,ptin:_RimLightColor,cmnt:--GroupMember RimLight,varname:_RimLightColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:7799,x:32215,y:32691,varname:node_7799,prsc:2|A-3475-OUT,B-6269-OUT;n:type:ShaderForge.SFN_NormalVector,id:111,x:30040,y:32744,prsc:2,pt:True;n:type:ShaderForge.SFN_Transform,id:7595,x:30987,y:31759,varname:node_7595,prsc:2,tffrom:0,tfto:3|IN-6154-OUT;n:type:ShaderForge.SFN_ComponentMask,id:3961,x:31361,y:31669,varname:node_3961,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-1623-OUT;n:type:ShaderForge.SFN_RemapRange,id:7802,x:31540,y:31669,varname:node_7802,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-3961-OUT;n:type:ShaderForge.SFN_Tex2d,id:886,x:31713,y:31669,ptovrint:False,ptlb:MatCap Texture,ptin:_MatCapTexture,cmnt:--GroupMember MatCap,varname:_MatCapTexture,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-7802-OUT;n:type:ShaderForge.SFN_Set,id:5033,x:31896,y:31669,varname:MatCap,prsc:2|IN-886-RGB;n:type:ShaderForge.SFN_Get,id:9250,x:33052,y:31294,varname:node_9250,prsc:2|IN-5033-OUT;n:type:ShaderForge.SFN_Add,id:9927,x:33287,y:32442,varname:node_9927,prsc:2|A-5588-OUT,B-9783-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:6169,x:33687,y:32294,ptovrint:False,ptlb:Enable MatCap,ptin:_EnableMatCap,cmnt:--Group MatCap,varname:_EnableMatCap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-5588-OUT,B-1021-OUT;n:type:ShaderForge.SFN_Clamp01,id:1575,x:33746,y:31907,varname:node_1575,prsc:2|IN-5714-OUT;n:type:ShaderForge.SFN_Clamp01,id:1021,x:33469,y:32442,varname:node_1021,prsc:2|IN-9927-OUT;n:type:ShaderForge.SFN_Clamp01,id:1981,x:31748,y:32707,varname:node_1981,prsc:2|IN-942-OUT;n:type:ShaderForge.SFN_Multiply,id:8471,x:33546,y:31169,varname:node_8471,prsc:2|A-7445-RGB,B-9250-OUT,C-181-OUT,D-246-OUT;n:type:ShaderForge.SFN_Slider,id:181,x:33015,y:31395,ptovrint:False,ptlb:MatCap Blend,ptin:_MatCapBlend,cmnt:--GroupMember MatCap,varname:_MatCapBlend,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:7445,x:33052,y:31143,ptovrint:False,ptlb:MatCap Color,ptin:_MatCapColor,cmnt:--GroupMember MatCap,varname:_MatCapColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Set,id:9183,x:33738,y:31169,varname:ColoredMatCap,prsc:2|IN-8471-OUT;n:type:ShaderForge.SFN_Get,id:4100,x:32775,y:32509,varname:node_4100,prsc:2|IN-9183-OUT;n:type:ShaderForge.SFN_Slider,id:6751,x:30492,y:32326,ptovrint:False,ptlb:Light Attenuation,ptin:_LightAttenuation,varname:_LightAttenuation,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Tex2d,id:9121,x:33368,y:32719,ptovrint:False,ptlb:NormalTex,ptin:_NormalTex,varname:_NormalTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Code,id:1428,x:30204,y:31377,varname:node_1428,prsc:2,code:cgBlAHQAdQByAG4AIABTAGgAYQBkAGUAUwBIADkAKABoAGEAbABmADQAKAAwAC4AMAAsACAAMQAuADAALAAgADAALgAwACwAIAAxAC4AMAApACkAOwAKAA==,output:2,fname:LightProbe,width:512,height:120;n:type:ShaderForge.SFN_Multiply,id:9324,x:33073,y:34564,varname:node_9324,prsc:2|A-8258-OUT,B-6569-OUT;n:type:ShaderForge.SFN_Slider,id:3216,x:32295,y:35059,ptovrint:False,ptlb:Outline Light Probe,ptin:_OutlineLightProbe,cmnt:--GroupMember Outline,varname:_OutlineLightProbe,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:1599,x:31130,y:33009,ptovrint:False,ptlb:Shade Softness,ptin:_ShadeSoftness,cmnt:--GroupMember Shade,varname:_ShadeSoftness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_OneMinus,id:7963,x:31667,y:33086,varname:node_7963,prsc:2|IN-4075-OUT;n:type:ShaderForge.SFN_Color,id:5738,x:31875,y:30532,ptovrint:False,ptlb:1st Shade Color,ptin:_1stShadeColor,cmnt:--GroupMember Shade --GroupMember ShadeColor,varname:_1stShadeColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_SwitchProperty,id:3166,x:33097,y:31813,ptovrint:False,ptlb:Enable Shade Color,ptin:_EnableShadeColor,cmnt:--GroupMember Shade --Group ShadeColor,varname:_EnableShadeColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-9917-OUT,B-2672-OUT;n:type:ShaderForge.SFN_RgbToHsv,id:8039,x:32105,y:31095,varname:node_8039,prsc:2|IN-9077-OUT;n:type:ShaderForge.SFN_Multiply,id:7823,x:32305,y:31202,varname:node_7823,prsc:2|A-8039-VOUT,B-9233-OUT;n:type:ShaderForge.SFN_HsvToRgb,id:7257,x:32503,y:31098,varname:node_7257,prsc:2|H-8039-HOUT,S-8039-SOUT,V-7823-OUT;n:type:ShaderForge.SFN_Clamp01,id:8091,x:32012,y:33235,varname:node_8091,prsc:2|IN-8430-OUT;n:type:ShaderForge.SFN_Multiply,id:9783,x:33053,y:32459,varname:node_9783,prsc:2|A-8653-OUT,B-4100-OUT;n:type:ShaderForge.SFN_Set,id:7901,x:31286,y:31209,varname:LightProbeValue,prsc:2|IN-3829-OUT;n:type:ShaderForge.SFN_Get,id:9637,x:31242,y:32114,varname:node_9637,prsc:2|IN-7901-OUT;n:type:ShaderForge.SFN_Multiply,id:2721,x:30906,y:32164,varname:node_2721,prsc:2|A-5928-OUT,B-6751-OUT;n:type:ShaderForge.SFN_Get,id:3028,x:32207,y:34903,varname:node_3028,prsc:2|IN-7901-OUT;n:type:ShaderForge.SFN_Clamp01,id:394,x:32399,y:32691,varname:node_394,prsc:2|IN-7799-OUT;n:type:ShaderForge.SFN_Smoothstep,id:8416,x:31466,y:33235,varname:node_8416,prsc:2|A-5708-OUT,B-3379-OUT,V-2284-OUT;n:type:ShaderForge.SFN_Slider,id:3749,x:30560,y:33093,ptovrint:False,ptlb:Shade Step,ptin:_ShadeStep,cmnt:--GroupMember Shade,varname:_ShadeStep,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Slider,id:105,x:30560,y:33228,ptovrint:False,ptlb:Shade Smooth,ptin:_ShadeSmooth,cmnt:--GroupMember Shade,varname:_ShadeSmooth,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_ValueProperty,id:4149,x:30666,y:33527,ptovrint:False,ptlb:Smooth Div,ptin:_SmoothDiv,varname:_SmoothDiv,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Divide,id:6392,x:30895,y:33235,varname:node_6392,prsc:2|A-105-OUT,B-4149-OUT;n:type:ShaderForge.SFN_Add,id:9145,x:31073,y:33235,varname:node_9145,prsc:2|A-3749-OUT,B-6392-OUT;n:type:ShaderForge.SFN_Subtract,id:8087,x:31073,y:33086,varname:node_8087,prsc:2|A-3749-OUT,B-6392-OUT;n:type:ShaderForge.SFN_Clamp01,id:3379,x:31287,y:33235,varname:node_3379,prsc:2|IN-9145-OUT;n:type:ShaderForge.SFN_Slider,id:3142,x:31708,y:32396,ptovrint:False,ptlb:RimLight Step,ptin:_RimLightStep,cmnt:--GroupMember RimLight,varname:_RimLightStep,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Slider,id:6687,x:31708,y:32555,ptovrint:False,ptlb:RimLight Smooth,ptin:_RimLightSmooth,cmnt:--GroupMember RimLight,varname:_RimLightSmooth,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_ValueProperty,id:5613,x:31787,y:32646,ptovrint:False,ptlb:SmoothDiv_copy,ptin:_SmoothDiv_copy,varname:_SmoothDiv_copy,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Divide,id:8141,x:32044,y:32554,varname:node_8141,prsc:2|A-6687-OUT,B-5613-OUT;n:type:ShaderForge.SFN_Add,id:9656,x:32222,y:32534,varname:node_9656,prsc:2|A-3142-OUT,B-8141-OUT;n:type:ShaderForge.SFN_Subtract,id:8543,x:32222,y:32396,varname:node_8543,prsc:2|A-3142-OUT,B-8141-OUT;n:type:ShaderForge.SFN_Clamp01,id:2258,x:32395,y:32396,varname:node_2258,prsc:2|IN-8543-OUT;n:type:ShaderForge.SFN_Clamp01,id:9478,x:32395,y:32534,varname:node_9478,prsc:2|IN-9656-OUT;n:type:ShaderForge.SFN_Smoothstep,id:5051,x:32585,y:32578,varname:node_5051,prsc:2|A-2258-OUT,B-9478-OUT,V-2817-OUT;n:type:ShaderForge.SFN_Multiply,id:4590,x:31896,y:31222,varname:node_4590,prsc:2|A-5687-OUT,B-3327-OUT;n:type:ShaderForge.SFN_Lerp,id:3474,x:32477,y:30351,varname:node_3474,prsc:2|A-5561-RGB,B-9615-OUT,T-6541-OUT;n:type:ShaderForge.SFN_OneMinus,id:9233,x:32105,y:31222,varname:node_9233,prsc:2|IN-4590-OUT;n:type:ShaderForge.SFN_Get,id:407,x:31854,y:30682,varname:node_407,prsc:2|IN-253-OUT;n:type:ShaderForge.SFN_Set,id:3667,x:33204,y:30636,varname:ShadowColoredShade,prsc:2|IN-1375-OUT;n:type:ShaderForge.SFN_Get,id:2672,x:32858,y:31849,varname:node_2672,prsc:2|IN-3667-OUT;n:type:ShaderForge.SFN_Get,id:7502,x:31521,y:31112,varname:node_7502,prsc:2|IN-6998-OUT;n:type:ShaderForge.SFN_OneMinus,id:5687,x:31688,y:31112,varname:node_5687,prsc:2|IN-7502-OUT;n:type:ShaderForge.SFN_Set,id:7629,x:32706,y:31098,varname:ShadowedTexure,prsc:2|IN-7257-OUT;n:type:ShaderForge.SFN_Get,id:9917,x:32858,y:31785,varname:node_9917,prsc:2|IN-7629-OUT;n:type:ShaderForge.SFN_Slider,id:7763,x:30274,y:31190,ptovrint:False,ptlb:Light Probe Softness,ptin:_LightProbeSoftness,varname:_LightProbeSoftness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Multiply,id:851,x:30880,y:31286,varname:node_851,prsc:2|A-1978-OUT,B-1428-OUT;n:type:ShaderForge.SFN_OneMinus,id:6186,x:30880,y:31147,varname:node_6186,prsc:2|IN-1978-OUT;n:type:ShaderForge.SFN_Add,id:3829,x:31097,y:31209,varname:node_3829,prsc:2|A-6186-OUT,B-851-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:160,x:33936,y:31839,ptovrint:False,ptlb:Enable RimLight,ptin:_EnableRimLight,cmnt:--Group RimLight,varname:_EnableRimLight,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-6708-OUT,B-1575-OUT;n:type:ShaderForge.SFN_Get,id:1604,x:33076,y:31699,varname:node_1604,prsc:2|IN-253-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:6708,x:33315,y:31838,ptovrint:False,ptlb:Enable Shade,ptin:_EnableShade,cmnt:--Group Shade,varname:_EnableShade,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-1604-OUT,B-3166-OUT;n:type:ShaderForge.SFN_ViewVector,id:9517,x:30637,y:31592,varname:node_9517,prsc:2;n:type:ShaderForge.SFN_Transform,id:2917,x:30811,y:31592,varname:node_2917,prsc:2,tffrom:0,tfto:3|IN-9517-OUT;n:type:ShaderForge.SFN_Vector3,id:2315,x:30811,y:31512,varname:node_2315,prsc:2,v1:-1,v2:-1,v3:1;n:type:ShaderForge.SFN_Multiply,id:1634,x:30987,y:31562,varname:node_1634,prsc:2|A-2315-OUT,B-2917-XYZ;n:type:ShaderForge.SFN_NormalBlend,id:1623,x:31176,y:31669,varname:node_1623,prsc:2|BSE-1634-OUT,DTL-7595-XYZ;n:type:ShaderForge.SFN_OneMinus,id:4075,x:31466,y:33086,varname:node_4075,prsc:2|IN-1599-OUT;n:type:ShaderForge.SFN_Multiply,id:4258,x:32895,y:32652,varname:node_4258,prsc:2|A-6197-OUT,B-3189-RGB,C-7181-OUT;n:type:ShaderForge.SFN_OneMinus,id:1978,x:30617,y:31193,varname:node_1978,prsc:2|IN-7763-OUT;n:type:ShaderForge.SFN_Clamp01,id:5708,x:31287,y:33086,varname:node_5708,prsc:2|IN-8087-OUT;n:type:ShaderForge.SFN_Multiply,id:5557,x:31669,y:33693,varname:node_5557,prsc:2|A-163-OUT,B-6121-OUT;n:type:ShaderForge.SFN_Add,id:2206,x:31840,y:33693,varname:node_2206,prsc:2|A-5557-OUT,B-809-OUT;n:type:ShaderForge.SFN_Set,id:489,x:32183,y:33693,varname:Shade2,prsc:2|IN-2188-OUT;n:type:ShaderForge.SFN_Slider,id:536,x:31130,y:34045,ptovrint:False,ptlb:2nd Shade Softness,ptin:_2ndShadeSoftness,cmnt:--GroupMember Shade --GroupMember ShadeColor,varname:_2ndShadeSoftness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_OneMinus,id:809,x:31669,y:33828,varname:node_809,prsc:2|IN-6121-OUT;n:type:ShaderForge.SFN_Clamp01,id:2188,x:32014,y:33693,varname:node_2188,prsc:2|IN-2206-OUT;n:type:ShaderForge.SFN_Smoothstep,id:163,x:31478,y:33693,varname:node_163,prsc:2|A-9215-OUT,B-3549-OUT,V-2284-OUT;n:type:ShaderForge.SFN_Divide,id:8448,x:30895,y:33842,varname:node_8448,prsc:2|A-2046-OUT,B-4149-OUT;n:type:ShaderForge.SFN_Add,id:6899,x:31073,y:33842,varname:node_6899,prsc:2|A-9260-OUT,B-8448-OUT;n:type:ShaderForge.SFN_Subtract,id:27,x:31073,y:33693,varname:node_27,prsc:2|A-9260-OUT,B-8448-OUT;n:type:ShaderForge.SFN_Clamp01,id:3549,x:31287,y:33842,varname:node_3549,prsc:2|IN-6899-OUT;n:type:ShaderForge.SFN_OneMinus,id:6121,x:31478,y:33828,varname:node_6121,prsc:2|IN-536-OUT;n:type:ShaderForge.SFN_Clamp01,id:9215,x:31287,y:33693,varname:node_9215,prsc:2|IN-27-OUT;n:type:ShaderForge.SFN_Lerp,id:9615,x:32269,y:30436,varname:node_9615,prsc:2|A-7646-RGB,B-5738-RGB,T-5302-OUT;n:type:ShaderForge.SFN_Color,id:7646,x:31875,y:30344,ptovrint:False,ptlb:2nd Shade Color,ptin:_2ndShadeColor,cmnt:--GroupMember Shade --GroupMember ShadeColor,varname:_2ndShadeColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Get,id:5302,x:32065,y:30578,varname:node_5302,prsc:2|IN-489-OUT;n:type:ShaderForge.SFN_Multiply,id:9260,x:30895,y:33693,varname:node_9260,prsc:2|A-3749-OUT,B-9327-OUT;n:type:ShaderForge.SFN_Slider,id:9327,x:30520,y:33711,ptovrint:False,ptlb:2nd Shade Ratio,ptin:_2ndShadeRatio,cmnt:--GroupMember Shade --GroupMember ShadeColor,varname:_2ndShadeRatio,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:2046,x:30520,y:33842,ptovrint:False,ptlb:2nd Shade Smooth,ptin:_2ndShadeSmooth,cmnt:--GroupMember Shade --GroupMember ShadeColor,varname:_2ndShadeSmooth,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:1352,x:32246,y:32998,ptovrint:False,ptlb:RimLight Softness,ptin:_RimLightSoftness,cmnt:--GroupMember RimLight,varname:_BlendRimLight,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Dot,id:942,x:31571,y:32703,varname:node_942,prsc:2,dt:4|A-5721-OUT,B-9678-OUT;n:type:ShaderForge.SFN_If,id:9441,x:30386,y:32702,varname:node_9441,prsc:2|A-1846-VFACE,B-9172-OUT,GT-111-OUT,EQ-111-OUT,LT-6677-OUT;n:type:ShaderForge.SFN_FaceSign,id:1846,x:30199,y:32535,varname:node_1846,prsc:2,fstp:0;n:type:ShaderForge.SFN_ValueProperty,id:9172,x:30199,y:32702,ptovrint:False,ptlb:FaceNormalConst,ptin:_FaceNormalConst,varname:_FaceNormalConst,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Negate,id:6677,x:30219,y:32800,varname:node_6677,prsc:2|IN-111-OUT;n:type:ShaderForge.SFN_Set,id:7726,x:30586,y:32702,varname:FaceNormal,prsc:2|IN-9441-OUT;n:type:ShaderForge.SFN_Get,id:6154,x:30790,y:31759,varname:node_6154,prsc:2|IN-7726-OUT;n:type:ShaderForge.SFN_Get,id:5721,x:31367,y:32689,varname:node_5721,prsc:2|IN-7726-OUT;n:type:ShaderForge.SFN_Get,id:3544,x:30884,y:33448,varname:node_3544,prsc:2|IN-7726-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7378,x:28925,y:32906,ptovrint:False,ptlb:RimLight Mask,ptin:_RimLightMask,cmnt:--GroupMember RimLight --Name RimLightMask --Enum None-Texture_Red-Texture_Green-Texture_Blue-Vertex_Red-Vertex_Green-Vertex_Blue,varname:_RimLightMask,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:6;n:type:ShaderForge.SFN_Set,id:1626,x:29668,y:32905,varname:RimLightMaskValue,prsc:2|IN-6749-OUT;n:type:ShaderForge.SFN_Code,id:6749,x:29354,y:32905,varname:node_6749,prsc:2,code:cwB3AGkAdABjAGgAIAAoAG0AbwBkAGUAKQAgAHsACgAgACAAYwBhAHMAZQAgADEAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgByADsACgAgACAAYwBhAHMAZQAgADIAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgBnADsACgAgACAAYwBhAHMAZQAgADMAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgBiADsACgAgACAAYwBhAHMAZQAgADQAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgByADsACgAgACAAYwBhAHMAZQAgADUAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgBnADsACgAgACAAYwBhAHMAZQAgADYAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgBiADsACgAgACAAZABlAGYAYQB1AGwAdAA6ACAAcgBlAHQAdQByAG4AIAAxADsACgB9AA==,output:0,fname:RimLightMaskSwitch,width:247,height:153,input:0,input:2,input:2,input_1_label:mode,input_2_label:tex,input_3_label:vertex|A-7378-OUT,B-2594-RGB,C-796-RGB;n:type:ShaderForge.SFN_Tex2d,id:2594,x:28925,y:33001,ptovrint:False,ptlb:RimLight Mask Tex,ptin:_RimLightMaskTex,cmnt:--GroupMember RimLight --If RimLightMask-1_3,varname:_RimLightMaskTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_VertexColor,id:796,x:29125,y:33034,varname:node_796,prsc:2;n:type:ShaderForge.SFN_Get,id:6269,x:32014,y:32904,varname:node_6269,prsc:2|IN-1626-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6221,x:28924,y:32585,ptovrint:False,ptlb:Shade Mask,ptin:_ShadeMask,cmnt:--GroupMember Shade --Name ShadeMask --Enum None-Texture_Red-Texture_Green-Texture_Blue-Vertex_Red-Vertex_Green-Vertex_Blue,varname:_ShadetMask,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:6;n:type:ShaderForge.SFN_Set,id:7063,x:29667,y:32584,varname:ShadeMaskValue,prsc:2|IN-969-OUT;n:type:ShaderForge.SFN_Code,id:969,x:29353,y:32584,varname:node_969,prsc:2,code:cwB3AGkAdABjAGgAIAAoAG0AbwBkAGUAKQAgAHsACgAgACAAYwBhAHMAZQAgADEAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgByADsACgAgACAAYwBhAHMAZQAgADIAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgBnADsACgAgACAAYwBhAHMAZQAgADMAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgBiADsACgAgACAAYwBhAHMAZQAgADQAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgByADsACgAgACAAYwBhAHMAZQAgADUAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgBnADsACgAgACAAYwBhAHMAZQAgADYAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgBiADsACgAgACAAZABlAGYAYQB1AGwAdAA6ACAAcgBlAHQAdQByAG4AIAAxADsACgB9AA==,output:0,fname:ShadeMaskSwitch,width:247,height:153,input:0,input:2,input:2,input_1_label:mode,input_2_label:tex,input_3_label:vertex|A-6221-OUT,B-3022-RGB,C-5680-RGB;n:type:ShaderForge.SFN_Tex2d,id:3022,x:28924,y:32680,ptovrint:False,ptlb:Shade Mask Tex,ptin:_ShadeMaskTex,cmnt:--GroupMember Shade --If ShadeMask-1_3,varname:_ShadeMaskTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_VertexColor,id:5680,x:29124,y:32713,varname:node_5680,prsc:2;n:type:ShaderForge.SFN_Set,id:5658,x:29667,y:33254,varname:OutlineMaskValue,prsc:2|IN-122-OUT;n:type:ShaderForge.SFN_Code,id:122,x:29353,y:33254,varname:node_122,prsc:2,code:cwB3AGkAdABjAGgAIAAoAG0AbwBkAGUAKQAgAHsACgAgACAAYwBhAHMAZQAgADEAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgByADsACgAgACAAYwBhAHMAZQAgADIAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgBnADsACgAgACAAYwBhAHMAZQAgADMAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgBiADsACgAgACAAYwBhAHMAZQAgADQAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgByADsACgAgACAAYwBhAHMAZQAgADUAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgBnADsACgAgACAAYwBhAHMAZQAgADYAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgBiADsACgAgACAAZABlAGYAYQB1AGwAdAA6ACAAcgBlAHQAdQByAG4AIAAxADsACgB9AA==,output:0,fname:OutlineMaskSwitch,width:247,height:153,input:0,input:2,input:2,input_1_label:mode,input_2_label:tex,input_3_label:vertex|A-6109-OUT,B-1529-RGB,C-7012-RGB;n:type:ShaderForge.SFN_VertexColor,id:7012,x:29124,y:33383,varname:node_7012,prsc:2;n:type:ShaderForge.SFN_Get,id:2326,x:32442,y:33920,varname:node_2326,prsc:2|IN-5658-OUT;n:type:ShaderForge.SFN_Get,id:3327,x:31688,y:31246,varname:node_3327,prsc:2|IN-7063-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6109,x:28924,y:33250,ptovrint:False,ptlb:Outline Mask,ptin:_OutlineMask,cmnt:--GroupMember Outline --Name OutlineMask --Enum None-Texture_Red-Texture_Green-Texture_Blue-Vertex_Red-Vertex_Green-Vertex_Blue,varname:node_6109,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:4;n:type:ShaderForge.SFN_Tex2d,id:1529,x:28924,y:33354,ptovrint:False,ptlb:Outline Mask Tex,ptin:_OutlineMaskTex,cmnt:--GroupMember Outline --If OutlineMask-1_3,varname:node_1529,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Code,id:5865,x:29355,y:33625,varname:node_5865,prsc:2,code:cwB3AGkAdABjAGgAIAAoAG0AbwBkAGUAKQAgAHsACgAgACAAYwBhAHMAZQAgADEAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgByADsACgAgACAAYwBhAHMAZQAgADIAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgBnADsACgAgACAAYwBhAHMAZQAgADMAOgAgAHIAZQB0AHUAcgBuACAAdABlAHgALgBiADsACgAgACAAYwBhAHMAZQAgADQAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgByADsACgAgACAAYwBhAHMAZQAgADUAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgBnADsACgAgACAAYwBhAHMAZQAgADYAOgAgAHIAZQB0AHUAcgBuACAAdgBlAHIAdABlAHgALgBiADsACgAgACAAZABlAGYAYQB1AGwAdAA6ACAAcgBlAHQAdQByAG4AIAAxADsACgB9AA==,output:0,fname:MatCapMaskSwitch,width:247,height:153,input:0,input:2,input:2,input_1_label:mode,input_2_label:tex,input_3_label:vertex|A-9383-OUT,B-9774-RGB,C-3443-RGB;n:type:ShaderForge.SFN_VertexColor,id:3443,x:29121,y:33784,varname:node_3443,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:9774,x:28929,y:33748,ptovrint:False,ptlb:MatCap Mask Tex,ptin:_MatCapMaskTex,cmnt:--GroupMember MatCap --If MatCapMask-1_3,varname:node_9774,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:9383,x:28929,y:33625,ptovrint:False,ptlb:MatCap Mask,ptin:_MatCapMask,cmnt:--GroupMember MatCap --Name MatCapMask --Enum None-Texture_Red-Texture_Green-Texture_Blue-Vertex_Red-Vertex_Green-Vertex_Blue,varname:node_9383,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Set,id:9178,x:29675,y:33627,varname:MatCapMaskValue,prsc:2|IN-5865-OUT;n:type:ShaderForge.SFN_Get,id:246,x:33151,y:31472,varname:node_246,prsc:2|IN-9178-OUT;n:type:ShaderForge.SFN_Clamp01,id:2284,x:31258,y:33481,varname:node_2284,prsc:2|IN-3597-OUT;n:type:ShaderForge.SFN_Multiply,id:3238,x:32656,y:34902,varname:node_3238,prsc:2|A-3729-OUT,B-3216-OUT;n:type:ShaderForge.SFN_OneMinus,id:3729,x:32384,y:34903,varname:node_3729,prsc:2|IN-3028-OUT;n:type:ShaderForge.SFN_OneMinus,id:6569,x:32864,y:34710,varname:node_6569,prsc:2|IN-3238-OUT;n:type:ShaderForge.SFN_OneMinus,id:7181,x:32585,y:32983,varname:node_7181,prsc:2|IN-1352-OUT;n:type:ShaderForge.SFN_OneMinus,id:6197,x:32744,y:32578,varname:node_6197,prsc:2|IN-5051-OUT;n:type:ShaderForge.SFN_OneMinus,id:2817,x:32324,y:32811,varname:node_2817,prsc:2|IN-394-OUT;n:type:ShaderForge.SFN_Code,id:1375,x:32908,y:30636,varname:node_1375,prsc:2,code:cwB3AGkAdABjAGgAIAAoAG0AbwBkAGUAKQAgAHsACgAgACAAYwBhAHMAZQAgADAAOgAgAHIAZQB0AHUAcgBuACAAaQBuAHAAdQB0ADEAOwAKACAAIABjAGEAcwBlACAAMQA6ACAAcgBlAHQAdQByAG4AIABpAG4AcAB1AHQAMgA7AAoAIAAgAGQAZQBmAGEAdQBsAHQAOgAgAHIAZQB0AHUAcgBuACAAMAA7AAoAfQA=,output:2,fname:ShadeModeSwitch,width:247,height:153,input:0,input:2,input:2,input_1_label:mode,input_2_label:input1,input_3_label:input2|A-5427-OUT,B-1065-OUT,C-8161-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5427,x:32656,y:30500,ptovrint:False,ptlb:Shade Mode,ptin:_ShadeMode,cmnt:--GroupMember Shade --GroupMember ShadeColor --Enum Multiply-Lerp,varname:node_5427,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:6541,x:32299,y:30805,varname:node_6541,prsc:2|A-9876-OUT,B-6389-OUT;n:type:ShaderForge.SFN_OneMinus,id:9876,x:32115,y:30716,varname:node_9876,prsc:2|IN-9451-OUT;n:type:ShaderForge.SFN_Color,id:5561,x:32269,y:30269,ptovrint:False,ptlb:ShadeMultiplyTemp,ptin:_ShadeMultiplyTemp,varname:node_5561,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:8161,x:32656,y:30697,varname:node_8161,prsc:2|A-407-OUT,B-9615-OUT,T-6541-OUT;n:type:ShaderForge.SFN_Multiply,id:1065,x:32656,y:30567,varname:node_1065,prsc:2|A-3474-OUT,B-407-OUT;n:type:ShaderForge.SFN_Get,id:6389,x:32094,y:30841,varname:node_6389,prsc:2|IN-7063-OUT;proporder:9212-9121-6751-7763-6708-6221-3022-3749-105-1599-3166-5427-5738-7646-9327-2046-536-160-7378-2594-3189-3142-6687-1352-2083-6109-1529-8648-8259-8893-8075-6450-3216-6169-9383-9774-886-7445-181-4149-5613-9172-5561;pass:END;sub:END;*/

Shader "Taremin/TareminShaderDevelop" {
    Properties {
        _MainTex ("MainTex", 2D) = "black" {}
        _NormalTex ("NormalTex", 2D) = "bump" {}
        _LightAttenuation ("Light Attenuation", Range(0, 1)) = 0.5
        _LightProbeSoftness ("Light Probe Softness", Range(0, 1)) = 0.5
        [MaterialToggle] _EnableShade ("Enable Shade", Float ) = 0
        _ShadeMask ("Shade Mask", Float ) = 6
        _ShadeMaskTex ("Shade Mask Tex", 2D) = "white" {}
        _ShadeStep ("Shade Step", Range(0, 1)) = 0.5
        _ShadeSmooth ("Shade Smooth", Range(0, 1)) = 0
        _ShadeSoftness ("Shade Softness", Range(0, 1)) = 0.5
        [MaterialToggle] _EnableShadeColor ("Enable Shade Color", Float ) = 0
        _ShadeMode ("Shade Mode", Float ) = 1
        _1stShadeColor ("1st Shade Color", Color) = (0.5,0.5,0.5,1)
        _2ndShadeColor ("2nd Shade Color", Color) = (0.5,0.5,0.5,1)
        _2ndShadeRatio ("2nd Shade Ratio", Range(-1, 1)) = 0
        _2ndShadeSmooth ("2nd Shade Smooth", Range(0, 1)) = 0
        _2ndShadeSoftness ("2nd Shade Softness", Range(0, 1)) = 0.5
        [MaterialToggle] _EnableRimLight ("Enable RimLight", Float ) = 0
        _RimLightMask ("RimLight Mask", Float ) = 6
        _RimLightMaskTex ("RimLight Mask Tex", 2D) = "white" {}
        _RimLightColor ("RimLight Color", Color) = (0,0,0,1)
        _RimLightStep ("RimLight Step", Range(0, 1)) = 0.5
        _RimLightSmooth ("RimLight Smooth", Range(0, 1)) = 1
        _RimLightSoftness ("RimLight Softness", Range(0, 1)) = 1
        [MaterialToggle] _EnableOutline ("Enable Outline", Float ) = 0
        _OutlineMask ("Outline Mask", Float ) = 4
        _OutlineMaskTex ("Outline Mask Tex", 2D) = "white" {}
        _OutlineWidth ("Outline Width", Range(0, 0.001)) = 0
        _OutlineColor ("Outline Color", Color) = (0.8382353,0.8382353,0.8382353,1)
        _OutlineBlendColor ("Outline Blend Color", Range(0, 1)) = 0
        _OutlineColorHueShift ("Outline Color Hue Shift", Range(0, 1)) = 0
        [HideInInspector]_OutlineColorHueShiftMod ("Outline Color Hue Shift Mod", Float ) = 1
        _OutlineLightProbe ("Outline Light Probe", Range(0, 1)) = 1
        [MaterialToggle] _EnableMatCap ("Enable MatCap", Float ) = 0
        _MatCapMask ("MatCap Mask", Float ) = 5
        _MatCapMaskTex ("MatCap Mask Tex", 2D) = "white" {}
        _MatCapTexture ("MatCap Texture", 2D) = "white" {}
        _MatCapColor ("MatCap Color", Color) = (1,1,1,1)
        _MatCapBlend ("MatCap Blend", Range(0, 1)) = 0
        [HideInInspector]_SmoothDiv ("Smooth Div", Float ) = 2
        [HideInInspector]_SmoothDiv_copy ("SmoothDiv_copy", Float ) = 2
        [HideInInspector]_FaceNormalConst ("FaceNormalConst", Float ) = 0.5
        [HideInInspector]_ShadeMultiplyTemp ("ShadeMultiplyTemp", Color) = (1,1,1,1)
        _Stencil ("Stencil ID", Float) = 0
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilComp ("Stencil Comparison", Float) = 8
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilOpFail ("Stencil Fail Operation", Float) = 0
        _StencilOpZFail ("Stencil Z-Fail Operation", Float) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _OutlineColor;
            uniform float _OutlineWidth;
            uniform float _OutlineBlendColor;
            uniform float _OutlineColorHueShift;
            float enableoutlinecode( fixed toggle , float value ){
            return toggle ? value : 0.0f;
            }
            
            uniform fixed _EnableOutline;
            uniform float _OutlineColorHueShiftMod;
            float3 LightProbe(){
            return ShadeSH9(half4(0.0, 1.0, 0.0, 1.0));
            
            }
            
            uniform float _OutlineLightProbe;
            uniform float _LightProbeSoftness;
            float OutlineMaskSwitch( float mode , float3 tex , float3 vertex ){
            switch (mode) {
              case 1: return tex.r;
              case 2: return tex.g;
              case 3: return tex.b;
              case 4: return vertex.r;
              case 5: return vertex.g;
              case 6: return vertex.b;
              default: return 1;
            }
            }
            
            uniform float _OutlineMask;
            uniform sampler2D _OutlineMaskTex; uniform float4 _OutlineMaskTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                float4 _OutlineMaskTex_var = tex2Dlod(_OutlineMaskTex,float4(TRANSFORM_TEX(o.uv0, _OutlineMaskTex),0.0,0)); // --GroupMember Outline --If OutlineMask-1_3
                float OutlineMaskValue = OutlineMaskSwitch( _OutlineMask , _OutlineMaskTex_var.rgb , o.vertexColor.rgb );
                float OutlineWidth = (OutlineMaskValue*_OutlineWidth);
                o.pos = UnityObjectToClipPos( float4(v.vertex.xyz + v.normal*enableoutlinecode( _EnableOutline , OutlineWidth ),1) );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 MainTexture = _MainTex_var.rgb;
                float3 node_91 = MainTexture;
                float4 node_3116_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_3116_p = lerp(float4(float4(node_91,0.0).zy, node_3116_k.wz), float4(float4(node_91,0.0).yz, node_3116_k.xy), step(float4(node_91,0.0).z, float4(node_91,0.0).y));
                float4 node_3116_q = lerp(float4(node_3116_p.xyw, float4(node_91,0.0).x), float4(float4(node_91,0.0).x, node_3116_p.yzx), step(node_3116_p.x, float4(node_91,0.0).x));
                float node_3116_d = node_3116_q.x - min(node_3116_q.w, node_3116_q.y);
                float node_3116_e = 1.0e-10;
                float3 node_3116 = float3(abs(node_3116_q.z + (node_3116_q.w - node_3116_q.y) / (6.0 * node_3116_d + node_3116_e)), node_3116_d / (node_3116_q.x + node_3116_e), node_3116_q.x);;
                float node_1978 = (1.0 - _LightProbeSoftness);
                float3 LightProbeValue = ((1.0 - node_1978)+(node_1978*LightProbe()));
                float3 OutlineColor = (lerp((lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac(fmod((node_3116.r+_OutlineColorHueShift),_OutlineColorHueShiftMod)+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_3116.g)*node_3116.b),_OutlineColor.rgb,_OutlineBlendColor)*(1.0 - ((1.0 - LightProbeValue)*_OutlineLightProbe)));
                return fixed4(OutlineColor,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            Stencil {
                Ref [_Stencil]
                ReadMask [_StencilReadMask]
                WriteMask [_StencilWriteMask]
                Comp [_StencilComp]
                Pass [_StencilOp]
                Fail [_StencilOpFail]
                ZFail [_StencilOpZFail]
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _RimLightColor;
            uniform sampler2D _MatCapTexture; uniform float4 _MatCapTexture_ST;
            uniform fixed _EnableMatCap;
            uniform float _MatCapBlend;
            uniform float4 _MatCapColor;
            uniform float _LightAttenuation;
            uniform sampler2D _NormalTex; uniform float4 _NormalTex_ST;
            float3 LightProbe(){
            return ShadeSH9(half4(0.0, 1.0, 0.0, 1.0));
            
            }
            
            uniform float _ShadeSoftness;
            uniform float4 _1stShadeColor;
            uniform fixed _EnableShadeColor;
            uniform float _ShadeStep;
            uniform float _ShadeSmooth;
            uniform float _SmoothDiv;
            uniform float _RimLightStep;
            uniform float _RimLightSmooth;
            uniform float _SmoothDiv_copy;
            uniform float _LightProbeSoftness;
            uniform fixed _EnableRimLight;
            uniform fixed _EnableShade;
            uniform float _2ndShadeSoftness;
            uniform float4 _2ndShadeColor;
            uniform float _2ndShadeRatio;
            uniform float _2ndShadeSmooth;
            uniform float _RimLightSoftness;
            uniform float _FaceNormalConst;
            uniform float _RimLightMask;
            float RimLightMaskSwitch( float mode , float3 tex , float3 vertex ){
            switch (mode) {
              case 1: return tex.r;
              case 2: return tex.g;
              case 3: return tex.b;
              case 4: return vertex.r;
              case 5: return vertex.g;
              case 6: return vertex.b;
              default: return 1;
            }
            }
            
            uniform sampler2D _RimLightMaskTex; uniform float4 _RimLightMaskTex_ST;
            uniform float _ShadeMask;
            float ShadeMaskSwitch( float mode , float3 tex , float3 vertex ){
            switch (mode) {
              case 1: return tex.r;
              case 2: return tex.g;
              case 3: return tex.b;
              case 4: return vertex.r;
              case 5: return vertex.g;
              case 6: return vertex.b;
              default: return 1;
            }
            }
            
            uniform sampler2D _ShadeMaskTex; uniform float4 _ShadeMaskTex_ST;
            float MatCapMaskSwitch( float mode , float3 tex , float3 vertex ){
            switch (mode) {
              case 1: return tex.r;
              case 2: return tex.g;
              case 3: return tex.b;
              case 4: return vertex.r;
              case 5: return vertex.g;
              case 6: return vertex.b;
              default: return 1;
            }
            }
            
            uniform sampler2D _MatCapMaskTex; uniform float4 _MatCapMaskTex_ST;
            uniform float _MatCapMask;
            float3 ShadeModeSwitch( float mode , float3 input1 , float3 input2 ){
            switch (mode) {
              case 0: return input1;
              case 1: return input2;
              default: return 0;
            }
            }
            
            uniform float _ShadeMode;
            uniform float4 _ShadeMultiplyTemp;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalTex_var = UnpackNormal(tex2D(_NormalTex,TRANSFORM_TEX(i.uv0, _NormalTex)));
                float3 normalLocal = _NormalTex_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 MainTexture = _MainTex_var.rgb;
                float3 node_9077 = MainTexture;
                float4 node_8039_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_8039_p = lerp(float4(float4(node_9077,0.0).zy, node_8039_k.wz), float4(float4(node_9077,0.0).yz, node_8039_k.xy), step(float4(node_9077,0.0).z, float4(node_9077,0.0).y));
                float4 node_8039_q = lerp(float4(node_8039_p.xyw, float4(node_9077,0.0).x), float4(float4(node_9077,0.0).x, node_8039_p.yzx), step(node_8039_p.x, float4(node_9077,0.0).x));
                float node_8039_d = node_8039_q.x - min(node_8039_q.w, node_8039_q.y);
                float node_8039_e = 1.0e-10;
                float3 node_8039 = float3(abs(node_8039_q.z + (node_8039_q.w - node_8039_q.y) / (6.0 * node_8039_d + node_8039_e)), node_8039_d / (node_8039_q.x + node_8039_e), node_8039_q.x);;
                float node_4075 = (1.0 - _ShadeSoftness);
                float node_6392 = (_ShadeSmooth/_SmoothDiv);
                float node_9441_if_leA = step(isFrontFace,_FaceNormalConst);
                float node_9441_if_leB = step(_FaceNormalConst,isFrontFace);
                float3 FaceNormal = lerp((node_9441_if_leA*(-1*normalDirection))+(node_9441_if_leB*normalDirection),normalDirection,node_9441_if_leA*node_9441_if_leB);
                float node_2284 = saturate(0.5*dot(FaceNormal,lightDirection)+0.5);
                float Shade = saturate(((1.0 - node_4075)+(node_4075*smoothstep( saturate((_ShadeStep-node_6392)), saturate((_ShadeStep+node_6392)), node_2284 ))));
                float4 _ShadeMaskTex_var = tex2D(_ShadeMaskTex,TRANSFORM_TEX(i.uv0, _ShadeMaskTex)); // --GroupMember Shade --If ShadeMask-1_3
                float ShadeMaskValue = ShadeMaskSwitch( _ShadeMask , _ShadeMaskTex_var.rgb , i.vertexColor.rgb );
                float3 ShadowedTexure = (lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac(node_8039.r+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_8039.g)*(node_8039.b*(1.0 - ((1.0 - Shade)*ShadeMaskValue))));
                float node_9260 = (_ShadeStep*_2ndShadeRatio);
                float node_8448 = (_2ndShadeSmooth/_SmoothDiv);
                float node_6121 = (1.0 - _2ndShadeSoftness);
                float Shade2 = saturate(((smoothstep( saturate((node_9260-node_8448)), saturate((node_9260+node_8448)), node_2284 )*node_6121)+(1.0 - node_6121)));
                float3 node_9615 = lerp(_2ndShadeColor.rgb,_1stShadeColor.rgb,Shade2);
                float node_6541 = ((1.0 - Shade)*ShadeMaskValue);
                float3 node_407 = MainTexture;
                float3 ShadowColoredShade = ShadeModeSwitch( _ShadeMode , (lerp(_ShadeMultiplyTemp.rgb,node_9615,node_6541)*node_407) , lerp(node_407,node_9615,node_6541) );
                float3 _EnableShade_var = lerp( MainTexture, lerp( ShadowedTexure, ShadowColoredShade, _EnableShadeColor ), _EnableShade ); // --Group Shade
                float node_8141 = (_RimLightSmooth/_SmoothDiv_copy);
                float4 _RimLightMaskTex_var = tex2D(_RimLightMaskTex,TRANSFORM_TEX(i.uv0, _RimLightMaskTex)); // --GroupMember RimLight --If RimLightMask-1_3
                float RimLightMaskValue = RimLightMaskSwitch( _RimLightMask , _RimLightMaskTex_var.rgb , i.vertexColor.rgb );
                float3 RimLight = ((1.0 - smoothstep( saturate((_RimLightStep-node_8141)), saturate((_RimLightStep+node_8141)), (1.0 - saturate(((1.0 - saturate(0.5*dot(FaceNormal,viewDirection)+0.5))*RimLightMaskValue))) ))*_RimLightColor.rgb*(1.0 - _RimLightSoftness));
                float3 ShadowedMainTexture = lerp( _EnableShade_var, saturate((_EnableShade_var+RimLight)), _EnableRimLight );
                float node_1978 = (1.0 - _LightProbeSoftness);
                float3 LightProbeValue = ((1.0 - node_1978)+(node_1978*LightProbe()));
                float3 DirectLighting = saturate((LightProbeValue+((attenuation*_LightAttenuation)*_LightColor0.rgb)));
                float3 node_8653 = DirectLighting;
                float3 node_5588 = (ShadowedMainTexture*node_8653);
                float3 node_1623_nrm_base = (float3(-1,-1,1)*mul( UNITY_MATRIX_V, float4(viewDirection,0) ).xyz.rgb) + float3(0,0,1);
                float3 node_1623_nrm_detail = mul( UNITY_MATRIX_V, float4(FaceNormal,0) ).xyz.rgb * float3(-1,-1,1);
                float3 node_1623_nrm_combined = node_1623_nrm_base*dot(node_1623_nrm_base, node_1623_nrm_detail)/node_1623_nrm_base.z - node_1623_nrm_detail;
                float3 node_1623 = node_1623_nrm_combined;
                float2 node_7802 = (node_1623.rg*0.5+0.5);
                float4 _MatCapTexture_var = tex2D(_MatCapTexture,TRANSFORM_TEX(node_7802, _MatCapTexture)); // --GroupMember MatCap
                float3 MatCap = _MatCapTexture_var.rgb;
                float4 _MatCapMaskTex_var = tex2D(_MatCapMaskTex,TRANSFORM_TEX(i.uv0, _MatCapMaskTex)); // --GroupMember MatCap --If MatCapMask-1_3
                float MatCapMaskValue = MatCapMaskSwitch( _MatCapMask , _MatCapMaskTex_var.rgb , i.vertexColor.rgb );
                float3 ColoredMatCap = (_MatCapColor.rgb*MatCap*_MatCapBlend*MatCapMaskValue);
                float3 Lighting = lerp( node_5588, saturate((node_5588+(node_8653*ColoredMatCap))), _EnableMatCap );
                float3 finalColor = Lighting;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _RimLightColor;
            uniform sampler2D _MatCapTexture; uniform float4 _MatCapTexture_ST;
            uniform fixed _EnableMatCap;
            uniform float _MatCapBlend;
            uniform float4 _MatCapColor;
            uniform float _LightAttenuation;
            uniform sampler2D _NormalTex; uniform float4 _NormalTex_ST;
            float3 LightProbe(){
            return ShadeSH9(half4(0.0, 1.0, 0.0, 1.0));
            
            }
            
            uniform float _ShadeSoftness;
            uniform float4 _1stShadeColor;
            uniform fixed _EnableShadeColor;
            uniform float _ShadeStep;
            uniform float _ShadeSmooth;
            uniform float _SmoothDiv;
            uniform float _RimLightStep;
            uniform float _RimLightSmooth;
            uniform float _SmoothDiv_copy;
            uniform float _LightProbeSoftness;
            uniform fixed _EnableRimLight;
            uniform fixed _EnableShade;
            uniform float _2ndShadeSoftness;
            uniform float4 _2ndShadeColor;
            uniform float _2ndShadeRatio;
            uniform float _2ndShadeSmooth;
            uniform float _RimLightSoftness;
            uniform float _FaceNormalConst;
            uniform float _RimLightMask;
            float RimLightMaskSwitch( float mode , float3 tex , float3 vertex ){
            switch (mode) {
              case 1: return tex.r;
              case 2: return tex.g;
              case 3: return tex.b;
              case 4: return vertex.r;
              case 5: return vertex.g;
              case 6: return vertex.b;
              default: return 1;
            }
            }
            
            uniform sampler2D _RimLightMaskTex; uniform float4 _RimLightMaskTex_ST;
            uniform float _ShadeMask;
            float ShadeMaskSwitch( float mode , float3 tex , float3 vertex ){
            switch (mode) {
              case 1: return tex.r;
              case 2: return tex.g;
              case 3: return tex.b;
              case 4: return vertex.r;
              case 5: return vertex.g;
              case 6: return vertex.b;
              default: return 1;
            }
            }
            
            uniform sampler2D _ShadeMaskTex; uniform float4 _ShadeMaskTex_ST;
            float MatCapMaskSwitch( float mode , float3 tex , float3 vertex ){
            switch (mode) {
              case 1: return tex.r;
              case 2: return tex.g;
              case 3: return tex.b;
              case 4: return vertex.r;
              case 5: return vertex.g;
              case 6: return vertex.b;
              default: return 1;
            }
            }
            
            uniform sampler2D _MatCapMaskTex; uniform float4 _MatCapMaskTex_ST;
            uniform float _MatCapMask;
            float3 ShadeModeSwitch( float mode , float3 input1 , float3 input2 ){
            switch (mode) {
              case 0: return input1;
              case 1: return input2;
              default: return 0;
            }
            }
            
            uniform float _ShadeMode;
            uniform float4 _ShadeMultiplyTemp;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalTex_var = UnpackNormal(tex2D(_NormalTex,TRANSFORM_TEX(i.uv0, _NormalTex)));
                float3 normalLocal = _NormalTex_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 MainTexture = _MainTex_var.rgb;
                float3 node_9077 = MainTexture;
                float4 node_8039_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_8039_p = lerp(float4(float4(node_9077,0.0).zy, node_8039_k.wz), float4(float4(node_9077,0.0).yz, node_8039_k.xy), step(float4(node_9077,0.0).z, float4(node_9077,0.0).y));
                float4 node_8039_q = lerp(float4(node_8039_p.xyw, float4(node_9077,0.0).x), float4(float4(node_9077,0.0).x, node_8039_p.yzx), step(node_8039_p.x, float4(node_9077,0.0).x));
                float node_8039_d = node_8039_q.x - min(node_8039_q.w, node_8039_q.y);
                float node_8039_e = 1.0e-10;
                float3 node_8039 = float3(abs(node_8039_q.z + (node_8039_q.w - node_8039_q.y) / (6.0 * node_8039_d + node_8039_e)), node_8039_d / (node_8039_q.x + node_8039_e), node_8039_q.x);;
                float node_4075 = (1.0 - _ShadeSoftness);
                float node_6392 = (_ShadeSmooth/_SmoothDiv);
                float node_9441_if_leA = step(isFrontFace,_FaceNormalConst);
                float node_9441_if_leB = step(_FaceNormalConst,isFrontFace);
                float3 FaceNormal = lerp((node_9441_if_leA*(-1*normalDirection))+(node_9441_if_leB*normalDirection),normalDirection,node_9441_if_leA*node_9441_if_leB);
                float node_2284 = saturate(0.5*dot(FaceNormal,lightDirection)+0.5);
                float Shade = saturate(((1.0 - node_4075)+(node_4075*smoothstep( saturate((_ShadeStep-node_6392)), saturate((_ShadeStep+node_6392)), node_2284 ))));
                float4 _ShadeMaskTex_var = tex2D(_ShadeMaskTex,TRANSFORM_TEX(i.uv0, _ShadeMaskTex)); // --GroupMember Shade --If ShadeMask-1_3
                float ShadeMaskValue = ShadeMaskSwitch( _ShadeMask , _ShadeMaskTex_var.rgb , i.vertexColor.rgb );
                float3 ShadowedTexure = (lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac(node_8039.r+float3(0.0,-1.0/3.0,1.0/3.0)))-1),node_8039.g)*(node_8039.b*(1.0 - ((1.0 - Shade)*ShadeMaskValue))));
                float node_9260 = (_ShadeStep*_2ndShadeRatio);
                float node_8448 = (_2ndShadeSmooth/_SmoothDiv);
                float node_6121 = (1.0 - _2ndShadeSoftness);
                float Shade2 = saturate(((smoothstep( saturate((node_9260-node_8448)), saturate((node_9260+node_8448)), node_2284 )*node_6121)+(1.0 - node_6121)));
                float3 node_9615 = lerp(_2ndShadeColor.rgb,_1stShadeColor.rgb,Shade2);
                float node_6541 = ((1.0 - Shade)*ShadeMaskValue);
                float3 node_407 = MainTexture;
                float3 ShadowColoredShade = ShadeModeSwitch( _ShadeMode , (lerp(_ShadeMultiplyTemp.rgb,node_9615,node_6541)*node_407) , lerp(node_407,node_9615,node_6541) );
                float3 _EnableShade_var = lerp( MainTexture, lerp( ShadowedTexure, ShadowColoredShade, _EnableShadeColor ), _EnableShade ); // --Group Shade
                float node_8141 = (_RimLightSmooth/_SmoothDiv_copy);
                float4 _RimLightMaskTex_var = tex2D(_RimLightMaskTex,TRANSFORM_TEX(i.uv0, _RimLightMaskTex)); // --GroupMember RimLight --If RimLightMask-1_3
                float RimLightMaskValue = RimLightMaskSwitch( _RimLightMask , _RimLightMaskTex_var.rgb , i.vertexColor.rgb );
                float3 RimLight = ((1.0 - smoothstep( saturate((_RimLightStep-node_8141)), saturate((_RimLightStep+node_8141)), (1.0 - saturate(((1.0 - saturate(0.5*dot(FaceNormal,viewDirection)+0.5))*RimLightMaskValue))) ))*_RimLightColor.rgb*(1.0 - _RimLightSoftness));
                float3 ShadowedMainTexture = lerp( _EnableShade_var, saturate((_EnableShade_var+RimLight)), _EnableRimLight );
                float node_1978 = (1.0 - _LightProbeSoftness);
                float3 LightProbeValue = ((1.0 - node_1978)+(node_1978*LightProbe()));
                float3 DirectLighting = saturate((LightProbeValue+((attenuation*_LightAttenuation)*_LightColor0.rgb)));
                float3 node_8653 = DirectLighting;
                float3 node_5588 = (ShadowedMainTexture*node_8653);
                float3 node_1623_nrm_base = (float3(-1,-1,1)*mul( UNITY_MATRIX_V, float4(viewDirection,0) ).xyz.rgb) + float3(0,0,1);
                float3 node_1623_nrm_detail = mul( UNITY_MATRIX_V, float4(FaceNormal,0) ).xyz.rgb * float3(-1,-1,1);
                float3 node_1623_nrm_combined = node_1623_nrm_base*dot(node_1623_nrm_base, node_1623_nrm_detail)/node_1623_nrm_base.z - node_1623_nrm_detail;
                float3 node_1623 = node_1623_nrm_combined;
                float2 node_7802 = (node_1623.rg*0.5+0.5);
                float4 _MatCapTexture_var = tex2D(_MatCapTexture,TRANSFORM_TEX(node_7802, _MatCapTexture)); // --GroupMember MatCap
                float3 MatCap = _MatCapTexture_var.rgb;
                float4 _MatCapMaskTex_var = tex2D(_MatCapMaskTex,TRANSFORM_TEX(i.uv0, _MatCapMaskTex)); // --GroupMember MatCap --If MatCapMask-1_3
                float MatCapMaskValue = MatCapMaskSwitch( _MatCapMask , _MatCapMaskTex_var.rgb , i.vertexColor.rgb );
                float3 ColoredMatCap = (_MatCapColor.rgb*MatCap*_MatCapBlend*MatCapMaskValue);
                float3 Lighting = lerp( node_5588, saturate((node_5588+(node_8653*ColoredMatCap))), _EnableMatCap );
                float3 finalColor = Lighting;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Unlit/Texture"
    CustomEditor "ShaderForgeMaterialInspector"
}
