# TareminShaderGUI

## TareminShaderGUI とは

ShaderForge 用のカスタムエディタです。
ShaderForge のノードにコメントを付けることでデフォルトより少しだけ見た目がよくなります。


## 基本的な使い方

1. Editor フォルダをプロジェクトにインポートする
2. ShaderForge でノードにコメントを付けていく
3. ShaderForge で作ったシェーダーの `CustomEditor "ShaderForgeMaterialInspector"` を `CustomEditor "TareminShaderGUI"` に書き換える

ただし、上記の書き換えはShaderForgeでシェーダーに変更を加えると元にもどってしまうので、以下のように自動化すると便利です。


### 書き換えの自動化

書き換え前のshaderファイルを開発用、書き換え後のshaderファイルを配布用として分けます。
開発中は開発用の shader ファイルを ShaderForge で設定して保存すると自動的に配布用の shader ファイルが更新されるようにします。

#### ReplaceOnAssetsUpdate

プロジェクトのアセットに変更があった場合にテキスト置換を行う下記のエディタ拡張を使います。

- https://github.com/Taremin/ReplaceOnAssetsUpdate から ReplaceOnAssetsUpdate をインストール

#### CustomEditor を書き換える JSON ファイルの作成

```json
{
	"replaceSettings": [
		{
			"sourcePathPattern": "\/[開発版のシェーダーファイル名]",
			"sourcePathReplace": "\/[配布版のシェーダーファイル名]",
			"replace": [
				{
					"textPattern": "CustomEditor \"ShaderForgeMaterialInspector\"",
					"textReplace": "CustomEditor \"TareminShaderGUI\""
				}
			]
		}
	]
}
```

## ノードに付けるコメントオプション

シェーダーの Property にコメントを付けることで設定画面を制御します。

ShaderForge で使えるコメントではほとんどの記号が制限されているため、コマンドラインオプション風の書式で Key-Value を取得します。
具体的には `--([a-zA-Z_0-9]+)\s+([a-zA-Z_0-9-]+)` の正規表現でマッチするものを取得しています。

### 名前の宣言

#### Group [名前]

名前の宣言をし、グループとして扱います。
基本的には `Switch` ノードなどにつけて、後述の `GroupMember` とセットとして使うことで、チェックボックスがOnのときだけグループメンバーを表示させるのに使います。
また、Groupでつけられたプロパティは上に行間が空き、書体が太字になります。

#### Name [名前]

名前の宣言を行います。
後述の `If` と組み合わせることで、グループよりも複雑な制御を行うことが出来ます。

### 表示条件

#### GroupMember [グループ名]

`Group` のついたプロパティの floatValue が 0 以外のときに表示します。

#### If [名前]-[Min]_[Max]

`Name` あるいは `Group` で宣言されたプロパティの floatValue が `Min` - `Max` のときに表示します。

### その他

#### Enum [Label1-Label2-...]

基本的に Value ノードにつけて、表示を Label1, Label2, ... の中から選択するようにします。
値は 0, 1, ... というようにインクリメントされます。

#### Bold 1

表示を太字にします。

#### Space 1

上に行間を開けます。


## License

[MIT](../LICENCE)
