using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

public class TareminShaderGUI : ShaderGUI {
    private bool groupBold = true;
    private bool groupSpace = true;
    protected Dictionary<string, Dictionary<string, List<string>>> nodeCommentOptions;
    bool stencilFoldout = false;

    private class KeywordToMaterialProperty {
        public string Keyword;
        public MaterialProperty Property;

        public KeywordToMaterialProperty (string keyword, MaterialProperty prop) {
            Keyword = keyword;
            Property = prop;
        }
    }

    void updateCommentOptions (MaterialEditor materialEditor) {
        nodeCommentOptions = new Dictionary<string, Dictionary<string, List<string>>> ();
        materialEditor.serializedObject.Update ();

        var shader = materialEditor.serializedObject.FindProperty ("m_Shader").objectReferenceValue as Shader;
        string[] shaderLines = File.ReadAllLines (AssetDatabase.GetAssetPath (shader));

        var sfDataRegex = new Regex (@"^/\*SF_DATA;(.*)\*/");
        foreach (var line in shaderLines) {
            var match = sfDataRegex.Match (line);
            if (!match.Success) {
                continue;
            }

            parseShaderForgeData (match.Groups[1].Value);
        }
    }

    void parseShaderForgeData (string shaderForgeData) {
        var rows = shaderForgeData.Split (';');

        foreach (var row in rows) {
            var keyValue = row.Split (new char[] { ':' }, 2);
            if (keyValue.Length < 2) {
                continue;
            }

            switch (keyValue[0]) {
                case "ver":
                    break;
                case "n":
                    var props = keyValue[1].Split (',');
                    var dict = new Dictionary<string, string> ();

                    foreach (var prop in props) {
                        var kv = prop.Split (new char[] { ':' }, 2);
                        if (kv.Length < 2) {
                            continue;
                        }
                        if (!dict.ContainsKey (kv[0])) {
                            dict.Add (kv[0], kv[1]);
                        }
                    }

                    if (dict.ContainsKey ("cmnt") && dict.ContainsKey ("ptin")) {
                        parseComment (dict["ptin"], dict["cmnt"]);
                    }
                    break;
            }
        }
    }

    void parseComment (string propertyName, string comment) {
        var pattern = new Regex (@"--([a-zA-Z_0-9]+)\s+([a-zA-Z_0-9-]+)");
        MatchCollection matches = pattern.Matches (comment);

        nodeCommentOptions[propertyName] = new Dictionary<string, List<string>> ();
        foreach (Match match in matches) {
            var key = match.Groups[1].Value;
            var value = match.Groups[2].Value;
            if (!nodeCommentOptions[propertyName].ContainsKey (key)) {
                nodeCommentOptions[propertyName][key] = new List<string> ();
            }
            nodeCommentOptions[propertyName][key].Add (value);
        }
    }

    override public void OnGUI (MaterialEditor materialEditor, MaterialProperty[] properties) {
        if (nodeCommentOptions == null) {
            updateCommentOptions (materialEditor);
        }

        var keywords = new List<KeywordToMaterialProperty> ();

        // save default style
        var baseLabelStyle = new GUIStyle (EditorStyles.label);
        var baseFoldoutStyle = new GUIStyle (EditorStyles.foldout);

        // process properties
        EditorGUI.BeginChangeCheck ();
        foreach (var propertyWithIndex in properties.Select ((property, index) => new { property, index })) {
            var property = propertyWithIndex.property;
            var index = propertyWithIndex.index;
            string[] drawEnum = null;
            var skip = false;
            var bold = false;
            var space = false;

            if ((property.flags & MaterialProperty.PropFlags.HideInInspector) != 0) {
                continue;
            }

            // process comment option
            if (nodeCommentOptions.ContainsKey (property.name)) {
                foreach (KeyValuePair<string, List<string>> kv in nodeCommentOptions[property.name]) {
                    foreach (var value in kv.Value) {
                        switch (kv.Key) {
                            case "Group":
                                if (groupBold) {
                                    bold = true;
                                }
                                if (groupSpace) {
                                    space = true;
                                }
                                goto case "Name";
                            case "Name":
                                keywords.Add (new KeywordToMaterialProperty (value, property));
                                break;
                            case "GroupMember":
                                foreach (var keyword in keywords) {
                                    if (value.Equals (keyword.Keyword) && keyword.Property.floatValue == 0) {
                                        skip = true;
                                    }
                                }
                                break;
                            case "Enum":
                                drawEnum = value.Split ('-');
                                break;
                            case "If":
                                var optionParams = value.Split (new char[] { '-' }, 2);
                                if (optionParams.Length < 2) {
                                    break;
                                }
                                foreach (var keyword in keywords) {
                                    if (optionParams[0].Equals (keyword.Keyword)) {
                                        var pattern = new Regex (@"(\d)+(?:_(\d+))?");
                                        Match match = pattern.Match (optionParams[1]);
                                        if (!match.Success) {
                                            break;
                                        }
                                        var min = System.Int32.Parse (match.Groups[1].Value);
                                        var max = match.Groups[2].Success ? System.Int32.Parse (match.Groups[2].Value) : min;
                                        if (keyword.Property.floatValue < min || keyword.Property.floatValue > max) {
                                            skip = true;
                                        }
                                    }
                                }
                                break;
                            case "Bold":
                                bold = true;
                                break;
                            case "Space":
                                space = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            // Stencil
            if (property.name.Equals ("_Stencil")) {
                EditorGUILayout.Space ();
                EditorStyles.foldout.fontStyle = FontStyle.Bold;
                stencilFoldout = EditorGUILayout.Foldout (stencilFoldout, "Stencil");
            }
            if (property.name.StartsWith ("_Stencil")) {
                skip = !stencilFoldout;

                System.Type type = null;
                switch (property.name) {
                    case "_StencilComp":
                        type = typeof (UnityEngine.Rendering.CompareFunction);
                        goto case "_StencilOp";
                    case "_StencilOp":
                    case "_StencilOpFail":
                    case "_StencilOpZFail":
                        type = type ?? typeof (UnityEngine.Rendering.StencilOp);
                        if (!skip) {
                            property.floatValue = EditorGUILayout.Popup (
                                property.displayName,
                                (int) property.floatValue,
                                System.Enum.GetNames (type)
                            );
                        }
                        skip = true;
                        break;
                }
            }

            if (space) {
                EditorGUILayout.Space ();
            }

            if (bold) {
                EditorStyles.label.fontStyle = FontStyle.Bold;
            }

            if (!skip) {
                if (drawEnum != null) {
                    property.floatValue = EditorGUILayout.Popup (
                        property.displayName,
                        (int) property.floatValue,
                        drawEnum
                    );
                } else {
                    materialEditor.ShaderProperty (property, property.displayName);
                }
            }

            // restore style
            EditorStyles.label.fontStyle = baseLabelStyle.fontStyle;
            EditorStyles.foldout.fontStyle = baseFoldoutStyle.fontStyle;
        }
    }
}